import jenkins
import time

qa_job = 'TestingHeadJobs'
prev_id = ''

j = jenkins.Jenkins('http://10.78.95.103:20180','admin','essenif')

if j.job_exists(qa_job):
    job_info = j.get_job_info(qa_job)
    print job_info
    success_build_number_before = job_info['lastCompletedBuild']['number']
    print('last_success_build_number Before Build:', success_build_number_before)

    print('Start job')
    j.build_job(qa_job)
    job_info = j.get_job_info(qa_job)
    print job_info
    time.sleep(10)

    while True:
        print('Waiting for build to start...')
        if prev_id != j.get_last_buildnumber():
            break
        time.sleep(3)
    print('Running...')
    last_build = j.get_last_build()
    while last_build.is_running():
        time.sleep(1)
    print(str(last_build.get_status()))

else:
    print('no job named {0} over there!!'.format(qa_job))
