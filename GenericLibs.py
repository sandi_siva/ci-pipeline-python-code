
import urllib2, json
import requests
import sys



def grep_json_(generarate_url):
    #print generarate_url
    getfailcount = ""
    getpassCount = ""
    percentage = ""
    result=""

    try:
        response = urllib2.urlopen(generarate_url,timeout=20)
        #print response
        data = response.readlines(80)
        #print data
        getfailcount = data[2]
        #print getfailcount
        #getfailcount[16:]
        #print getfailcount[:-2]
        getpassCount = data[3]

        result= []
        result.append(getfailcount)
        result.append(getpassCount)

        #print result
    except :
        print ""

    percentage,total , getPass , getfail  = get_ercetage_cal(getfailcount, getpassCount)
    return result , percentage , total ,getPass ,getfail
    # print "jenkinsurl1 : Run Functional Tests -UCCE-OUTBOUND-API"  +getfailcount + getpassCount
#This is only for Catwoman as its new Jenkins and the Json structure is little differennt



def grep_json_catwoman(generarate_url):
   # print generarate_url
    getfailcount = ""
    getpassCount = ""
    percentage = ""
    result=""

    try:
        response = urllib2.urlopen(generarate_url)
        data = response.readlines(150)
        getfailcount = data[7]
        getpassCount = data[8]

        result= []
        result.append(getfailcount)
        result.append(getpassCount)

        #print result
    except:
        print "Looks like job is still Running, Run after the job is completed for latestSuccessfulBuild Result"

    percentage, total , getPass ,getfail  = get_ercetage_cal(getfailcount, getpassCount)
    return result , percentage ,total , getPass , getfail


def get_ercetage_cal(failc,passc):
    percentage = ""
    total = ""
    passcount = ""
    failcount = ""
    try:
        sp = failc.split(':')
        ab = sp[1]
        ab3 = ab[:-2]

        sp1 = passc.split(':')
        ab1 = sp1[1]
        ab2 = ab1[:-2]
        #print "Pass:",ab2 ,"Fail :",ab3
        total = int(ab2) + int(ab3)

        #print total
        percentage = 100.0 * int(ab2) / total
        passcount = int(ab2)
        failcount = int(ab3)
    except:
        print ""
    #print "Pass Percentage is  :" ,percentage
    return percentage ,total , passcount ,failcount

def _average_atomation_percent(passtotaltests , totaltests):
    try:
        print "=========================================================================="
        print 'TOTAL TESTS  : ' , totaltests
        print 'PASS TESTS : ' , passtotaltests
        print "AVERAGE PASS PERCENTAGE  : " , 100.0 * int(passtotaltests) / totaltests
        print "========================================================================="
    except:
        print "Please re-run to get the overall Percentage, All jobs are not yet completed..."

    if (100.0 * int(passtotaltests) / totaltests) < 94.0:
        print "Below Benchmark - http://10.232.38.135:8080/job/GetAutomationResults/"
        sys.exit(-1)

def get_build_number(jenkinsname,jenkinsjob):

    _url = jenkinsname + "/job/" + jenkinsjob + "/lastBuild/api/json?pretty=true"
    try:
        r = requests.get(_url, timeout=10)

        data = r.json()
        if data["result"] == "FAILURE" :
            return "Upgrade Failed"
        else:
            return data["description"]
    except:
        return "Check the Test Bed"



def get_date_of_lastBuild(jenkinsname,jenkinsjob):
    _url = jenkinsname + "/job/" + jenkinsjob + "/lastBuild/api/json?pretty=true"
    try:
        r = requests.get(_url, timeout=10)

        data = r.json()
        value = data["id"]
        return value[:-9]
    except:
        return "Date aint working"


def get_state_of_job(jenkinsname,jenkinsjob):
    _url = jenkinsname + "/job/" + jenkinsjob + "/lastBuild/api/json?pretty=true"
    try:
        r = requests.get(_url, timeout=10)

        data = r.json()
        value = data["building"]
        #print value
        if value == 'true':

            return value
    except:
        return value

def get_date_catwoman(jenkinsname,jenkinsjob):
    _url = jenkinsname + "/job/" + jenkinsjob + "/lastBuild/buildTimestamp"
    try:
        r = requests.get(_url, timeout=10)
        return r.text[:-7]
    except:
        return "Date aint working"




