import urllib2, json
import requests
#from DumbDatatoDB import dump_result_to_database
from GenericLibs import grep_json_,get_build_number,get_date_of_lastBuild,get_state_of_job
from prettytable import PrettyTable
from exceptions import Exception

#from DumbDatatoDB import create_csv
from tabulate import tabulate

jenkinsurl1 = "http://ip:port/"
jenkinsurl2 = "http://ip:port/"


jenkins1_JOBNAMES = {'job1': 'Run%20Functional%20Tests1',
                   'job2' : 'Run%20Functional%20Tests2',
                   'job3':'Run%20Functional%20Tests3'}

jenkins2_JOBNAMES = {'job2': 'Run%20Functional%20Tests1',
                       'job3': 'Run%20Functional%20Tests2',
                       'job4': 'Run%20Functional%20Tests3'
                       }
passresult = []


_BUILD_JOB_NAMES = 'Finesse%20Upgrade%20(VOS)%20-%20Primary'


def grep_automation_testresults():

    #List of Test Servers
    api = "api/json/?pretty=true"

    t = PrettyTable(['Jenkins','Build Number','Date' ,'URL','Pass','Fail','Percentage'])
    headers = ["Jenkins","Buldnum"]
    for key in jenkins1_JOBNAMES:
        jenkinsurl1_url = jenkinsurl1 + "/job/" + jenkins1_JOBNAMES[key] + "/lastBuild/testReport/"
        jenkinsurl1_build = jenkinsurl1 + "/job" + _BUILD_JOB_NAMES

        result , percent ,total , getpass , getfail  = grep_json_(jenkinsurl1_url+api)
        try:
            if get_build_number(jenkinsurl1,_BUILD_JOB_NAMES) ==  "Upgrade Failed":
                t.add_row(['jenkinsurl1',get_build_number(jenkinsurl1, _BUILD_JOB_NAMES),get_date_of_lastBuild(jenkinsurl1, _BUILD_JOB_NAMES),"","","",""])

            else:
                r = get_state_of_job(jenkinsurl1,jenkins1_JOBNAMES[key])

                t.add_row(['jenkinsurl1',get_build_number(jenkinsurl1,_BUILD_JOB_NAMES),get_date_of_lastBuild(jenkinsurl1, _BUILD_JOB_NAMES),jenkinsurl1_url,getpass,getfail,percent])

                list.append(total)
                passresult.append(getpass)

        except:
            print "jenkinsurl1 - Upgrade in Progress"


	
    #t.add_row(["------","------","------","------","------","-----","------"])
    #t.add_row([" Total Tests","","","",sum(passresult),"",overall_percent])

    print t

    try:
        totaltests = sum(list)
        passtotaltests = sum(passresult)
        overall_percent = 100.0 * int(passtotaltests) / totaltests
        #print overall_percent.isdigit()

	#t.add_row(["------","------","------","------","------","-----","------"])
        #t.add_row([" Total Tests","","","",sum(passresult),"",overall_percent])
        print "-----------------------------------------------------------------------"
        print "UCCX Pass Percentage :::" ,overall_percent
	print "Total Tests          :::" , totaltests
        print "Total Pass           :::", passtotaltests
        print "----------------------------------------------------------------------"
        
        f = open ("/jenkins_results/percent.txt",'r+b')
	f.seek(0)
        f.truncate()
	f.write(str(overall_percent))
        f.close()


    except:
        print "***********************************************************"
	print " Maybe Jobs are still running or someting is not right..."
        print "***********************************************************"


if __name__ == "__main__":
    grep_automation_testresults()
